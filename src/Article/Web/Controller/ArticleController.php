<?php

declare(strict_types=1);

namespace App\Article\Web\Controller;

use App\Article\Web\Adapter\LoadArticlePort;
use App\Common\DashboardController;
use App\Common\Pagination\PagerDto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ArticleController extends DashboardController
{
    private LoadArticlePort $loadArticlePort;

    public function __construct(
        LoadArticlePort     $loadArticlePort,
        TranslatorInterface $translator
    )
    {
        $this->loadArticlePort = $loadArticlePort;

        parent::__construct($translator);
    }

    /**
     * @Route ("/actions", name="articles")
     */
    public function articlesPage(Request $request): Response
    {
        $page = (int) $request->get('page');

        $pager = $this->loadArticlePort->getPaginated(($page or $page==0) ? 1 : $page);

        return $this->render('admin/article/index.html.twig', ['pager' => $pager, 'user' => $this->getUser()]);
    }
}

