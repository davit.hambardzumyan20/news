<?php

namespace App\Article\Web\Controller;

use App\Article\Web\Adapter\LoadArticlePort;
use App\Article\Web\Adapter\UpdateArticleStatePort;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;

class DeleteArticleController extends AbstractController
{
    private LoadArticlePort $loadArticlePort;

    private UpdateArticleStatePort $articleStatePort;

    public function __construct(
        LoadArticlePort     $loadArticlePort,
        UpdateArticleStatePort $articleStatePort,
        TranslatorInterface $translator
    )
    {
        $this->loadArticlePort = $loadArticlePort;
        $this->articleStatePort = $articleStatePort;
    }

    /**
     * @Route ("/articles/{id}", methods={"DELETE"}, name="delete_article")
     */
    public function deleteArticles(string $id): Response
    {
        $article = $this->loadArticlePort->getById($id);

        $this->articleStatePort->delete($article);

//        $pager = $this->loadArticlePort->getPaginated($page ?? 1);

        return new JsonResponse(['status' => 'deleted']);
    }
}