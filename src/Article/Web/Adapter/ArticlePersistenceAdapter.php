<?php

namespace App\Article\Web\Adapter;

use App\Article\Domain\Article;
use App\User\Domain\User;
use Doctrine\ORM\EntityManagerInterface;

class ArticlePersistenceAdapter implements UpdateArticleStatePort
{
	private EntityManagerInterface $entityManager;

	/**
	 * UserPersistenceAdapter constructor.
	 *
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * {@inheritDoc}
	 */
	public function save(Article $article): void
	{
		$this->entityManager->persist($article);
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete(Article $article): void
	{
		$this->entityManager->remove($article);
	}
}
