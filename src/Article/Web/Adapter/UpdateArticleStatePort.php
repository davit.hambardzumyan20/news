<?php

namespace App\Article\Web\Adapter;

use App\Article\Domain\Article;
use App\User\Domain\User;

interface UpdateArticleStatePort
{
    /**
     * @param Article $article
     */
	public function save(Article $article): void;

    /**
     * @param Article $article
     */
    public function delete(Article $article): void;
}
