<?php

namespace App\Article\Web\Adapter;

use App\Article\Domain\Article;
use App\Common\Pagination\PagerDto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleRepository implements LoadArticlePort
{
    /**
     * @var ServiceEntityRepository
     */
    private ServiceEntityRepository $serviceEntityRepository;

    const LIMIT = 10;

    /**
     * NoteRepository constructor.
     *
     * @param ServiceEntityRepository $serviceEntityRepository
     */
    public function __construct(ServiceEntityRepository $serviceEntityRepository)
    {
        $this->serviceEntityRepository = $serviceEntityRepository;
    }

    public function getById(string $articleId): Article
    {
        $article = $this->serviceEntityRepository->findOneBy(['id' => $articleId]);

        if (!$article instanceof Article) {
            throw new NotFoundHttpException(
                'Article not found,'
            );
        }

        return $article;
    }

    public function getAll(): array
    {
        return $this->serviceEntityRepository->findAll();
    }

    public function findByName(string $name): ?Article
    {
        return $this->serviceEntityRepository->findOneBy(['name' => $name]);
    }

    public function getPaginated(int $page): PagerDto
    {
        $query = $this->serviceEntityRepository->createQueryBuilder('a');

        $items = $this->paginate($query, $page);

        return new PagerDto(
            $items,
            $page,
            count($items) / self::LIMIT
        );
    }

    public function paginate($dql, $page = 1)
    {
        $paginator = new Paginator($dql);

        $paginator->setUseOutputWalkers(false);

        $paginator->getQuery()
            ->setFirstResult(self::LIMIT * ($page - 1)) // Offset
            ->setMaxResults(self::LIMIT); // Limit

        return $paginator;
    }

}
