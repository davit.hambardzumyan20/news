<?php

namespace App\Article\Web\Adapter;

use App\Article\Domain\Article;
use App\Common\Pagination\PagerDto;

interface LoadArticlePort
{
    public function getById(string $articleId): Article;

    public function getAll(): array;

    public function findByName(string $name): ?Article;

    public function getPaginated(int $page): PagerDto;
}
