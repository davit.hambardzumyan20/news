<?php

declare(strict_types=1);

namespace App\Article\Domain;

use App\Article\Application\Command\UpdateArticleCommand;
use App\Common\DateTimeUserCase;
use App\Common\UuidGeneratorInterface;

class ArticleFactory
{
    private UuidGeneratorInterface $uuidGenerator;

    private DateTimeUserCase $timeUserCase;

    public function __construct(
        UuidGeneratorInterface $uuidGenerator,
        DateTimeUserCase $timeUserCase
    ) {
        $this->uuidGenerator = $uuidGenerator;
        $this->timeUserCase = $timeUserCase;
    }

    public function create(
        UpdateArticleCommand $articleCommand
    ) {
        return new Article(
            $this->uuidGenerator->getUuid(),
            $articleCommand->getName(),
            $articleCommand->getShortDescription(),
            $articleCommand->getPicture(),
            $this->timeUserCase->now()
        );
    }
}

