<?php

namespace App\Article\Domain\Async;

use App\Article\Application\Command\UpdateArticleCommand;
use App\Article\Application\SaveArticleService;

class UpdateArticleMessage
{
    private UpdateArticleCommand $command;

    public function __construct(UpdateArticleCommand $command)
    {
        $this->command = $command;
    }

    /**
     * @return UpdateArticleCommand
     */
    public function getCommand(): UpdateArticleCommand
    {
        return $this->command;
    }
}