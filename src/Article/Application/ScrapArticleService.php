<?php

declare(strict_types=1);

namespace App\Article\Application;

use App\Article\Application\Command\UpdateArticleCommand;
use App\Article\Application\UseCase\ScrapArticleInterface;
use App\Article\Application\UseCase\ScraperInterface;
use App\Article\Domain\Article;
use App\Article\Domain\Async\UpdateArticleMessage;
use Symfony\Component\Messenger\MessageBusInterface;

class ScrapArticleService implements ScrapArticleInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @param ScraperInterface $scraper
     *
     * @return void
     */
    public
    function scrapArticles(ScraperInterface $scraper): void
    {
        while (true) {
            $articles = $scraper->scrap();

            foreach ($articles as $article) {
                $this->messageBus->dispatch(
                    new UpdateArticleMessage($article)
                );
            }

            if (empty($articles)) {
                break;
            }

            $scraper->next();
        }
    }
}

