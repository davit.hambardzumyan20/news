<?php

declare(strict_types=1);

namespace App\Article\Application\UseCase;

use App\Article\Application\Command\UpdateArticleCommand;
use App\Article\Application\Dto\ScrapOptions;

interface ScraperInterface
{
    /**
     * @return UpdateArticleCommand[]
     */
    public function scrap(): array;

    public function setOptions(?ScrapOptions $options): void;

    public function next(): void;
}
