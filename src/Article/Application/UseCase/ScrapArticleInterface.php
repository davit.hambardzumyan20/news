<?php

declare(strict_types=1);

namespace App\Article\Application\UseCase;

use App\Article\Application\Command\UpdateArticleCommand;
use App\Article\Domain\Article;

interface ScrapArticleInterface
{
    /**
     * @param ScraperInterface $scraper
     *
     * @return void
     */
    public function scrapArticles(ScraperInterface $scraper): void;
}
