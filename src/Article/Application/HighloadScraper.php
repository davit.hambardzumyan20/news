<?php

declare(strict_types=1);

namespace App\Article\Application;

use App\Article\Application\Command\UpdateArticleCommand;
use App\Article\Application\Dto\ScrapOptions;
use App\Article\Application\UseCase\ScraperInterface;
use DOMDocument;
use DOMElement;
use DOMXPath;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class HighloadScraper implements ScraperInterface
{
    private ?ScrapOptions $options = null;

    private Client $client;

    const RESOURCE_URL = "https://highload.today/wp-content/themes/supermc/ajax/loadarchive.php";

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function setOptions(?ScrapOptions $options): void
    {
        $this->options = $options;
    }

    /**
     * @return UpdateArticleCommand[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function scrap(): array
    {
        $commands = [];

        $response = $this->getData();
        $body = $response->getBody()->getContents();
        $body = str_replace('\\t', '', $body);

        $items = preg_split('<div class="lenta-item">', $body);
        array_shift($items);

        foreach ($items as $item) {
            try {
                $name = null;
                $shortDescription = null;
                $picture = null;

                $shortDescription = preg_split('[<p>]', $item);
                $shortDescription = preg_split('[</p>]', $shortDescription[1])[0];
                $picture = preg_split('[src="]', $item);
                $picture = preg_split('["]', $picture[2] ?? $picture[1])[0];
                $name = preg_split('[<h2>]', $item);
                $name = preg_split('[</h2>]', $name[1])[0];

                $commands[] = new UpdateArticleCommand(
                    $name,
                    $shortDescription,
                    $picture
                );

            } catch (\Exception $exception) {
                print('Wrong data provided in HighloadScraper');
                print_r($exception->getMessage());
            }
        }

        return $commands;

//        $doc = new DOMDocument();
//        $doc->loadHTML($body);
//        var_dump($items[1]);
//        $keywords = preg_split('[<p>]', $items[1]);
//
//        dd($keywords);
//
//        $xpath = new DOMXPath($doc);
//
//        $itemElements = $xpath->evaluate('//div[@class="lenta-item"]');
//
//        foreach ($itemElements as $element) {
//
//            $pTags = $element->getElementsByTagName('p');
//
//            foreach ($pTags as $pTag) {
//                echo($pTag->nodeValue);
//            }
//            $imgTags = $element->getElementsByTagName('img');
//            $h2Tags = $element->getElementsByTagName('h2');
//            $commands[] = new UpdateArticleCommand(
//                $h2Tags[0]->nodeValue,
//                $pTags[2]->nodeValue,
//                $imgTags[0]->nodeValue
//            );
//
//            return $commands;
//        }
//
//        return [];
    }

    public function getOptions(): ScrapOptions
    {
        return $this->options;
    }

    private function getData(): ResponseInterface
    {


        if (is_null($this->options)) {
            $request = new Request('POST', self::RESOURCE_URL, [], );

            return $this->client->send($request, $this->options->getOptions());
        }

        return $this->client->post(self::RESOURCE_URL, $this->options->getOptions());
    }

    public function next(): void
    {
        if (!is_null($this->options)) {
            $this->options->next();
        }
    }
}

