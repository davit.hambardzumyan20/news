<?php

namespace App\Article\Application\Command;

class UpdateArticleCommand
{
    private string $name;

    private string $shortDescription;

    private string $picture;

    public function __construct(
        string $name,
        string $shortDescription,
        string $picture
    ) {
        $this->name = $name;
        $this->shortDescription = $shortDescription;
        $this->picture = $picture;
    }

    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    public function getPicture(): string
    {
        return $this->picture;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
