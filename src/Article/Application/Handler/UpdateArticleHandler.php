<?php

namespace App\Article\Application\Handler;

use App\Article\Application\SaveArticleService;
use App\Article\Domain\Async\UpdateArticleMessage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class UpdateArticleHandler implements MessageHandlerInterface
{
    private SaveArticleService $saveArticleService;

    private EntityManagerInterface $entityManager;

    private LoggerInterface $logger;

    const MAX_MEMORY_USAGE = 120000;

    public function __construct(
        SaveArticleService     $saveArticleService,
        EntityManagerInterface $entityManager,
        LoggerInterface        $logger
    )
    {
        $this->saveArticleService = $saveArticleService;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function __invoke(UpdateArticleMessage $updateArticleMessage)
    {
        $memoryUsage = memory_get_usage(true) / 1024;

        $this->logger->info('Memory usage - ' . memory_get_usage(true) / 1024 . ' MB.');
        if ($memoryUsage > self::MAX_MEMORY_USAGE) {
            $this->logger->error('Memory usage - ' . $memoryUsage . ' MB.');
            sleep(1);
        } elseif ($memoryUsage > self::MAX_MEMORY_USAGE * 0.9) {
            $this->logger->warning('Memory usage - ' . $memoryUsage . ' MB.');
        } else {
            $this->logger->info('Memory usage - ' . $memoryUsage . ' MB.');
        }

        $this->saveArticleService->update(
            $updateArticleMessage->getCommand()
        );

        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}