<?php

declare(strict_types=1);

namespace App\Article\Application\Dto;

class ScrapOptions
{
    private int $page;

    private int $pageLimit;

    public function __construct(int $page, int $pageLimit)
    {
        $this->page = $page;
        $this->pageLimit = $pageLimit;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPageLimit(): int
    {
        return $this->pageLimit;
    }

    public function getOptions(): array
    {
        return [
            'multipart' => [
                [
                    'name' => 'page',
                    'contents' => $this->page
                ],
                [
                    'name' => 'stick',
                    'contents' => $this->pageLimit
                ]
            ]];
    }

    public function next(): void
    {
        $this->page = $this->page + 1;
    }
}

