<?php

namespace App\Article\Application;

use App\Article\Application\Command\UpdateArticleCommand;
use App\Article\Domain\Article;
use App\Article\Domain\ArticleFactory;
use App\Article\Web\Adapter\LoadArticlePort;
use App\Article\Web\Adapter\UpdateArticleStatePort;
use App\Common\DateTimeUserCase;

class SaveArticleService
{
    private LoadArticlePort $loadArticlePort;

    private UpdateArticleStatePort $articleStatePort;

    private ArticleFactory $articleFactory;

    private DateTimeUserCase $dateTimeUserCase;

    public function __construct(
        LoadArticlePort $loadArticlePort,
        UpdateArticleStatePort $articleStatePort,
        ArticleFactory $articleFactory,
        DateTimeUserCase $dateTimeUserCase
    ) {
        $this->loadArticlePort = $loadArticlePort;
        $this->articleStatePort = $articleStatePort;
        $this->articleFactory = $articleFactory;
        $this->dateTimeUserCase = $dateTimeUserCase;
    }

    public function update(UpdateArticleCommand $articleCommand): Article
    {
        $article = $this->loadArticlePort->findByName($articleCommand->getName());

        if (is_null($article)) {
            $article = $this->articleFactory->create($articleCommand);
            $this->articleStatePort->save($article);

            return $article;
        }

        $article->setShortDescription($articleCommand->getShortDescription());
        $article->setPicture($articleCommand->getPicture());
        $article->setUpdatedAt($this->dateTimeUserCase->now());

        return $article;
    }
}
