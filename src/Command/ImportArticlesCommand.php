<?php

namespace App\Command;

use App\Article\Application\Dto\ScrapOptions;
use App\Article\Application\HighloadScraper;
use App\Article\Application\SaveArticleService;
use App\Article\Application\UseCase\ScrapArticleInterface;
use App\Article\Domain\Async\UpdateArticleMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ImportArticlesCommand extends Command
{
    private ScrapArticleInterface $scrapArticle;

    private HighloadScraper $highloadScraper;


    const LIMIT = 35;

    const START_PAGE = 1;

    protected static $defaultName = 'app:articles:import';

    public function __construct(
        ScrapArticleInterface $scrapArticle,
        HighloadScraper $highloadScraper,
        MessageBusInterface $messageBus
    )
    {
        $this->scrapArticle = $scrapArticle;
        $this->highloadScraper = $highloadScraper;
        $this->messageBus = $messageBus;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Scraping articles and saving in database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $options = new ScrapOptions(1, 35);

        $this->highloadScraper->setOptions($options);

        $this->scrapArticle->scrapArticles($this->highloadScraper);

        return Command::SUCCESS;
    }
}
