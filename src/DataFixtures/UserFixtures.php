<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\User\Application\CreateUserCommand;
use App\User\Domain\UserFactory;
use App\User\Web\Adapter\UpdateUserStatePort;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\User\Domain\User;
use DateTime;

class UserFixtures extends Fixture
{
    private const TEST_PASSWORD = 'test';

    private UserPasswordHasherInterface $passwordEncoder;

    private UserFactory $userFactory;

    private UpdateUserStatePort $updateUserStatePort;

    public function __construct(
        UserPasswordHasherInterface $encoder,
        UserFactory $userFactory,
        UpdateUserStatePort $updateUserStatePort
    ) {
        $this->passwordEncoder = $encoder;
        $this->userFactory = $userFactory;
        $this->updateUserStatePort = $updateUserStatePort;
    }

    public static function getGroups(): array
    {
        return ['demo'];
    }

    /**
     * @param ObjectManager $manager
     *
     * @return void
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        // add Admin User
        $createUserCommand = new CreateUserCommand(
            'admin',
            'admin',
            'admin@email.com',
            '+37493333333',
            'admin',
                    [User::ROLE_ADMIN]
        );

        $user = $this->updateUserStatePort->save(
            $this->userFactory->create(
                $createUserCommand
            )
        );


        $hashPassword = $this->passwordEncoder->hashPassword($user, static::TEST_PASSWORD);

        $user->setPassword($hashPassword);

        // add moderator
        $createUserCommand = new CreateUserCommand(
            'moderator',
            'moderator',
            'moderator@email.com',
            '+37493333333',
            'moderator',
            [User::ROLE_MODERATOR]
        );

        $user = $this->updateUserStatePort->save(
            $this->userFactory->create(
                $createUserCommand
            )
        );

        #ToDo: change hashing logic
        $hashPassword = $this->passwordEncoder->hashPassword($user, static::TEST_PASSWORD);

        $user->setPassword($hashPassword);

        $manager->flush();
    }
}
