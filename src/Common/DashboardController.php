<?php

declare(strict_types=1);

namespace App\Common;

use App\User\Domain\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractDashboardController
{
    private TranslatorInterface $translator;

    public function __construct(
        TranslatorInterface $translator
    )
    {
        $this->translator = $translator;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()->setTitle('Test project');
    }

    public function configureAssets(): Assets
    {
        $assets = Assets::new();


        $assets->addCssFile('https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
        $assets->addCssFile('/index.css');
        $assets->addJsFile('/js/main.js');

        return $assets;
    }

    public function configureMenuItems(): iterable
    {
        $loggedInUser = $this->getUser();

        yield MenuItem::linkToDashboard($this->translator->trans('Dashboard'), 'fa fa-home');
        yield MenuItem::linkToRoute($this->translator->trans('Article'), 'fa fa-file-text', 'articles');
    }

    /**
     * @Route("/admin", methods={"GET"}, name="admin")
     */
    public function index(): Response
    {
        /** @var User $loggedInUser */
        $loggedInUser = $this->getUser();

        return $this->render(
            'admin/base.html.twig',
            [
                "loggedInUser" => $loggedInUser
            ]
        );
    }
}
