<?php

namespace App\Common\Pagination;

use Doctrine\ORM\Tools\Pagination\Paginator;

class PagerDto
{
    public Paginator $items;

    public int $page;

    public int $maxPage;

    public ?int $previous = null;

    public ?int $next = null;

    public array $pages = [];

    public function __construct(Paginator $items, int $page, int $maxPage)
    {
        $this->items = $items;
        $this->page = $page;
        $this->maxPage = $maxPage;

        if ($page > 1 and $maxPage > 10) {
            $this->previous = $page - 1;
        }
        if ($page < $maxPage) {
            $this->next = $page + 1;
        }

        for ($i = 1; $i < $maxPage; ++$i) {
            if ((($i - $page) * ($i - $page)) < 25) {
                $this->pages[] = $i;
            }
        }
//        dd($this->pages);
    }
}