<?php

declare(strict_types=1);

namespace App\Common;

use DateTime;

class DateTimeUserCase
{
    public function now(): DateTime
    {
        return new DateTime('now');
    }
}

