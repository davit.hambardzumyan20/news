<?php

declare(strict_types=1);

namespace App\Common\Controller;

use App\User\Web\Adapter\LoadUserPort;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

class SecurityController extends AbstractController
{
    private LoadUserPort $userRepository;
    private TokenStorageInterface $tokenStorage;

    public function __construct(
        LoadUserPort $userRepository,
        TokenStorageInterface $tokenStorage)
    {
        $this->userRepository = $userRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route ("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route ("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new LogicException(
            'This method can be blank - it will be intercepted by the logout key on your firewall.'
        );
    }

    /**
     * @Route ("/loginExtern", name="loginExtern")
     */
    public function loginExtern(Request $request): RedirectResponse
    {
        $base64 = $request->get('token');

        $data = json_decode(base64_decode($base64), true);

        $userId = $data['userId'];

        $user = $this->userRepository->getById($userId);

        //Handle getting or creating the user entity likely with a posted form
        // The third parameter "main" can change according to the name of your firewall in security.yml
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->tokenStorage->setToken($token);

        // If the firewall name is not main, then the set value would be instead:
        //$session = $request->getSession();
        $request->getSession()->set('_security_main', serialize($token));

        // Fire the login event manually
        $dispatcher = new EventDispatcher();
        $event = new InteractiveLoginEvent($request, $token);
        $dispatcher->dispatch($event, "security.interactive_login");

        /*
         * Now the user is authenticated !
         */
        return new RedirectResponse("admin");
    }
}
