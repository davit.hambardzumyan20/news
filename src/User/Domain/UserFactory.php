<?php

namespace App\User\Domain;

use App\Common\UuidGeneratorInterface;
use App\User\Application\CreateUserCommand;
use DateTime;

class UserFactory
{
	private UuidGeneratorInterface $uuidGenerator;

	public function __construct(UuidGeneratorInterface $uuidGenerator)
	{
		$this->uuidGenerator = $uuidGenerator;
	}

	public function create(CreateUserCommand $command)
	{
        return new User(
            $this->uuidGenerator->getUuid(),
            $command->getName(),
            $command->getUsername(),
            $command->getPassword(),
            $command->getRoles(),
            new DateTime('now')
        );
	}
}
