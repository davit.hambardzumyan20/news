<?php

namespace App\User\Domain;

use App\Common\UuidInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @method string getUserIdentifier()
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    const ROLE_MODERATOR = 'ROLE_MODERATOR';
    const ROLE_ADMIN = 'ROLE_ADMIN';

	/**
	 * @ORM\Id
	 * @ORM\Column(type="guid")
	 */
	private string $id;

	/**
	 * @ORM\Column(type="string", length=180, unique=true)
	 */
	private string $name;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private string $username;

    /**
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $createdAt;

	public function __construct(
		UuidInterface $id,
		string $name,
		string $userName,
		string $password,
        array $roles,
        DateTimeInterface $createdAt
	) {
		$this->id = $id->toString();
		$this->name = $name;
        $this->username = $userName;
        $this->password = $password;
        $this->roles = $roles;
        $this->createdAt = $createdAt;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}


	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

    public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function isModerator(): bool
    {
        return in_array(self::ROLE_MODERATOR, $this->roles);
    }
}
