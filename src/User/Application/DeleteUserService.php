<?php

namespace App\User\Application;

use App\User\Domain\User;
use App\User\Web\Adapter\UserPersistenceAdapter;

class DeleteUserService implements DeleteUserUseCase
{
	public UserPersistenceAdapter $persistenceAdapter;

	public function __construct(UserPersistenceAdapter $persistenceAdapter)
	{
		$this->persistenceAdapter = $persistenceAdapter;
	}

	public function delete(User $user)
	{
		$this->persistenceAdapter->delete($user);
	}
}