<?php

namespace App\User\Application;

class UpdateUserService implements UpdateUserUseCase
{
	public function update(UpdateUserCommand $command): void
	{
		$user = $command->getUser();

		$user->setEmail($command->getEmail());
		$user->setName($command->getName());

		$userInfo = $user->getUserInfo();
		$userInfo->setPassport($command->getPassport());
		$userInfo->setPhone($command->getPhone());

		$user->setUserInfo($userInfo);
	}
}