<?php

namespace App\User\Application;

class CreateUserCommand
{
	private string $email;

	private string $name;

	private string $username;

	private string $phone;

	private string $password;

    private array $roles = ['ROLE_USER'];

	public function __construct(
		string $name,
        string $username,
		string $email,
		string $phone,
		string $password,
        array $roles
	) {
		$this->email = $email;
		$this->name = $name;
        $this->username = $username;
		$this->phone = $phone;
		$this->password = $password;
        $this->roles = $roles;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getPhone(): string
	{
		return $this->phone;
	}

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}
