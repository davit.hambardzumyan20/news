<?php

namespace App\User\Web\Request;

class CreateUserForm
{
	public string $Name;

	public string $Email;

	public string $Phone;

	public string $Passport;
}