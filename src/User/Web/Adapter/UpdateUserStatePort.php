<?php

namespace App\User\Web\Adapter;


use App\User\Domain\User;

interface UpdateUserStatePort
{
	/**
	 * @param User $user
	 */
	public function save(User $user): User;
}
