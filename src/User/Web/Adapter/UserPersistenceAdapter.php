<?php

namespace App\User\Web\Adapter;


use App\User\Domain\User;
use Doctrine\ORM\EntityManagerInterface;

class UserPersistenceAdapter implements UpdateUserStatePort
{
	private EntityManagerInterface $entityManager;

	/**
	 * UserPersistenceAdapter constructor.
	 *
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * {@inheritDoc}
	 */
	public function save(User $user): User
	{
		$this->entityManager->persist($user);

        return $user;
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete(User $user): void
	{
		$this->entityManager->remove($user);
	}
}
