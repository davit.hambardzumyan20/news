const users = document.getElementById("users");

if (users) {
    users.addEventListener('click', e => {
        if (e.target.className === 'btn btn-danger delete-user') {
            const id = e.target.getAttribute('data-id');
            fetch(`/user/${id}`, {
                method: 'DELETE'
            }).then(res => window.location.reload());
        }
    });
}

function deleteArticle(id) {
    const url = '/articles/' + id

    const response = fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-type': 'application/json'
        }
    }).then(function () {
        // location.reload();
    });
    console.log(id)
}