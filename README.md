# Task
### Requirements
1- docker-compose

2- make

## Getting started

```make
make start
```
create docker containers
```make
make update 
```
Run all migrations 
```make
make doctrine-fixtures-load
```
Load test data in database for login as admin
    
Environments for login

## APP

### ADMIN_ROLE
```
username - admin
password - test
```

### MODERATOR_ROLE
```
username - moderator
password - test
```

## PHP_MY_ADMIN
```
username - root
password - root
```

## RABBITMQ
```
username - rabbitmq
password - rabbitmq
```

## If something is wrong, check your local ports

### Run workers and cron jobs

```make
make scrap-articles
```
Scraping data and save it in RabbitMQ

```make
make run-workers
```
Running workers and save or updated data in MySQL
